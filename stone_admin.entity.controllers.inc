<?php

/**
 * Admin UI controller class for bundleable content.
 */
class StoneEntityBundleableUIController extends EntityBundleableUIController {
  public function hook_menu() {
    $items = parent::hook_menu();

    // Redefine the list page since a super class unset it.
    $plural_label = isset($this->entityInfo['plural label']) ? $this->entityInfo['plural label'] : $this->entityInfo['label'] . 's';
    $plural_label = ucfirst($plural_label);

    $items[$this->path] = array(
      'title' => $plural_label,
      'description' => "Manage $plural_label.",
      'page callback' => 'drupal_get_form',
      'page arguments' => array($this->entityType . '_overview_form', $this->entityType),
      'access arguments' => array('administer ' . $this->entityType),
      'file' => 'includes/entity.ui.inc',
    );
    $items[$this->path . '/list'] = array(
      'title' => 'List',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    // Devel module integration.
    if (module_exists('devel')) {
      $items[$this->path . '/%entity_object/devel'] = array(
        'title' => 'Devel',
        'page callback' => 'devel_load_object',
        'page arguments' => array($this->entityType, $this->id_count),
        'access arguments' => array('access devel information'),
        'type' => MENU_LOCAL_TASK,
        'file' => 'devel.pages.inc',
        'file path' => drupal_get_path('module', 'devel'),
        'weight' => 10,
      );
      $items[$this->path . '/%entity_object/devel/load'] = array(
        'title' => 'Load',
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );

      $items[$this->path . '/%entity_object/devel/render'] = array(
        'title' => 'Render',
        'page callback' => 'devel_render_object',
        'page arguments' => array($this->entityType, $this->id_count),
        'access arguments' => array('access devel information'),
        'file' => 'devel.pages.inc',
        'file path' => drupal_get_path('module', 'devel'),
        'type' => MENU_LOCAL_TASK,
        'weight' => 10,
      );
    }

    return $items;
  }

  protected function operationCount() {
    // We only have edit and delete in our setup.
    return 2;
  }

  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $entity_uri = entity_uri($this->entityType, $entity);

    $row[] = array('data' => array(
      '#theme' => 'entity_ui_overview_item',
      '#label' => entity_label($this->entityType, $entity),
      '#name' => !empty($this->entityInfo['exportable']) ? entity_id($this->entityType, $entity) : FALSE,
      '#url' => $entity_uri ? $entity_uri : FALSE,
      '#entity_type' => $this->entityType,
    ));

    $row[] = l('edit', $this->path . '/' . $id . '/edit', array(
      'query' => drupal_get_destination(),
    ));

    $row[] = l('delete', $this->path . '/' . $id . '/delete');
    return $row;
  }

  public function operationFormSubmit($form, &$form_state) {
    $msg = $this->applyOperation($form_state['op'], $form_state[$this->entityType]);
    drupal_set_message($msg);
    $form_state['redirect'] = $this->path;
  }
}
